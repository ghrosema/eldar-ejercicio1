public abstract class Brand {

    private String brand;
    private Double rate;


    public Double getRate(Double money){
        return money * rate;
    };
    public abstract void calculateRate();

    public String getBrand() {
        return brand;
    }

    public void setBrand(String brand) {
        this.brand = brand;
    }

    public Double getRate() {
        return rate;
    }

    public void setRate(Double rate) {
        this.rate = rate;
    }

    @Override
    public String toString() {
        return "Brand{" +
                "brand='" + brand + '\'' +
                ", rate=" + rate +
                '}';
    }
}
