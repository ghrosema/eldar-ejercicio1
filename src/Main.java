import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.GregorianCalendar;

public class Main {
    public static void main(String[] args){

        CardHolder yo = new CardHolder("Rodrigo", "Márquez");
        CreditCard tarjeta1 = new CreditCard("1010101010101010",new Date(122,10,2), yo, new Visa());
        CreditCard tarjeta2 = new CreditCard("1010101010101013",new Date(124,1,24), yo, new Amex());
        CreditCard tarjeta3 = new CreditCard("1010101010101017",new Date(124,11,20), yo, new Nara());

        System.out.println(tarjeta1.toString());
        System.out.println(tarjeta2.toString());
        System.out.println(tarjeta3.toString());

        tarjeta1.performOperation(8000.00);
        tarjeta1.comparateCard(tarjeta2);
        tarjeta1.validateExpirationDate();
        tarjeta2.validateExpirationDate();
        tarjeta1.calculateRateOperation(800.00);
    }
}