import java.time.LocalDate;

public class Nara extends Brand{

    public Nara() {
        this.setBrand("NARA");
    }
    @Override
    public void calculateRate() {
        Double dayOfMonth = (double) LocalDate.now().getDayOfMonth();
        this.setRate(dayOfMonth * 0.5);
    }
}
