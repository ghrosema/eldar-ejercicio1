import java.time.LocalDate;
import java.util.Date;

public class Visa extends Brand{

    public Visa() {
        this.setBrand("VISA");
    }
    @Override
    public void calculateRate() {
        Double month = (double) LocalDate.now().getMonthValue();
        Double year = LocalDate.now().getYear() % 100.00;
        this.setRate(year / month);
    }
}
