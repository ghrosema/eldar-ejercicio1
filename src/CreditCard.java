import java.util.Date;

public class CreditCard {
    private String number;
    private Date expiration;
    private CardHolder cardHolder;
    private Brand brand;

    private Double limit;


    public CreditCard(String number, Date expiration, CardHolder cardHolder, Brand brand) {
        this.number = number;
        this.expiration = expiration;
        this.cardHolder = cardHolder;
        this.brand = brand;
        this.limit = 1000.00;
        this.brand.calculateRate();
    }

    public CreditCard(String number) {
        this.number = number;
    }

    public String getNumber() {
        return number;
    }

    public Date getExpiration() {
        return expiration;
    }

    public CardHolder getCardHolder() {
        return cardHolder;
    }

    public Brand getBrand() {
        return brand;
    }

    public void performOperation(Double money){
        if (money < limit){
            System.out.println("Valid operation");
        }else{
            System.out.println("Not valid operation");
        }
    }

    public void comparateCard(CreditCard creditCArd){
        if(this.number.equals(creditCArd.getNumber())){
            System.out.println("It is the same card");
        }else{
            System.out.println("Those are different cards");
        }
    }
    public void validateExpirationDate(){
        int status = this.expiration.compareTo(new Date());
        if(status >= 0){
            System.out.println("The credit card has expired");
        }else{
            System.out.println("The credit card has not expired");
        }
    }

    public void calculateRateOperation(Double money){
        System.out.println(this.brand.getRate() * money);
    }

    @Override
    public String toString() {
        return "CreditCard{" +
                "number='" + number + '\'' +
                ", expiration=" + expiration +
                ", cardHolder=" + cardHolder.toString() +
                ", brand=" + brand.toString() +
                '}';
    }
}
