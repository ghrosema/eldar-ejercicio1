import java.time.LocalDate;

public class Amex extends Brand{

    public Amex() {
        this.setBrand("AMEX");
    }
    @Override
    public void calculateRate() {
        Double month = (double) LocalDate.now().getMonthValue();
        this.setRate(month * 0.1);
    }
}
